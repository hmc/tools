# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2018-2020 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package Build::Utils;

=head1 NAME

Build::Utils - Utility functions for the build scripts

=cut

use Exporter 'import';
our @EXPORT = qw(GetToolName InfoMsg LogMsg Error
                 GitPull ApplyPatch
                 GetCPUCount BuildNativeTestAgentd BuildWindowsTestAgentd
                 GetTestLauncher BuildTestLauncher BuildWine UpdateAddOns
                 SetupWineEnvironment GetWineDir GetTestDir RunWine
                 CreateWinePrefix);

use Digest::SHA;
use File::Basename;
use File::Path;

use WineTestBot::Config;
use WineTestBot::Missions;
use WineTestBot::PatchUtils;
use WineTestBot::Utils;

WineTestBot::Config::PrependPaths("/usr/lib/ccache");


#
# Logging and error handling
#

my $Name0 = $0;
$Name0 =~ s+^.*/++;

sub GetToolName()
{
  return $Name0;
}

sub InfoMsg(@)
{
  print @_;
}

sub LogMsg(@)
{
  print "Task: ", @_;
}

sub Error(@)
{
  print STDERR "$Name0:error: ", @_;
}


#
# Repository updates
#

sub GitPull($)
{
  my ($Dir) = @_;

  if (!-d "$DataDir/$Dir")
  {
    InfoMsg "\nRetrieving the $Dir source\n";
    system("set -x && git clone $RepoURLs{$Dir}");
  }
  else
  {
    InfoMsg "\nUpdating the $Dir source\n";
    system("cd '$DataDir/$Dir' && (set -x && git pull --rebase) && ".
           "echo $Dir:HEAD=`git rev-parse HEAD`");
  }
  if ($? != 0)
  {
    LogMsg "Git clone/pull failed\n";
    return !1;
  }

  if ($Dir eq "wine")
  {
    my $ErrMessage = UpdateWineData("$DataDir/$Dir");
    if ($ErrMessage)
    {
      LogMsg "$ErrMessage\n";
      return !1;
    }
  }

  return 1;
}

sub ApplyPatch($$)
{
  my ($Dir, $PatchFile) = @_;

  InfoMsg "Applying patch\n";
  system("cd '$DataDir/$Dir' && ".
         "echo $Dir:HEAD=`git rev-parse HEAD` && ".
         "set -x && ".
         "git apply --verbose ". ShQuote($PatchFile) ." && ".
         "git add -A");
  if ($? != 0)
  {
    LogMsg "Patch failed to apply\n";
    return undef;
  }

  my $Impacts = GetPatchImpacts($PatchFile);
  if ($Impacts->{MakeRequests})
  {
    InfoMsg "\nRunning make_requests\n";
    system("cd '$DataDir/$Dir' && set -x && ./tools/make_requests");
    if ($? != 0)
    {
      LogMsg "make_requests failed\n";
      return undef;
    }
  }
  if ($Impacts->{MakeErrors})
  {
    InfoMsg "\nRunning make_errors\n";
    system("cd '$DataDir/$Dir/' && set -x && ./dlls/ntdll/make_errors");
    if ($? != 0)
    {
      LogMsg "make_errors failed\n";
      return undef;
    }
  }
  if ($Impacts->{MakeFir})
  {
    InfoMsg "\nRunning make_fir\n";
    system("cd '$DataDir/$Dir/dlls/dsound/' && set -x && ./make_fir");
    if ($? != 0)
    {
      LogMsg "make_fir failed\n";
      return undef;
    }
  }
  if ($Impacts->{MakeOpenGL})
  {
    InfoMsg "\nRunning make_opengl\n";
    system("cd '$DataDir/$Dir/dlls/opengl32' && set -x && ./make_opengl");
    if ($? != 0)
    {
      LogMsg "make_opengl failed\n";
      return undef;
    }
  }
  if ($Impacts->{MakeUnicode})
  {
    InfoMsg "\nRunning make_unicode\n";
    system("cd '$DataDir/$Dir/' && set -x && ./tools/make_unicode");
    if ($? != 0)
    {
      LogMsg "make_unicode failed\n";
      return undef;
    }
  }
  if ($Impacts->{MakeVulkan})
  {
    InfoMsg "\nRunning make_vulkan\n";
    system("cd '$DataDir/$Dir' && set -x && ./dlls/winevulkan/make_vulkan");
    if ($? != 0)
    {
      LogMsg "make_vulkan failed\n";
      return undef;
    }
  }

  if ($Impacts->{MakeMakefiles})
  {
    InfoMsg "\nRunning make_makefiles\n";
    system("cd '$DataDir/$Dir' && set -x && ./tools/make_makefiles");
    if ($? != 0)
    {
      LogMsg "make_makefiles failed\n";
      return undef;
    }
  }

  if ($Impacts->{Autoconf})
  {
    InfoMsg "\nRunning autoreconf\n";
    system("cd '$DataDir/$Dir' && set -x && autoreconf");
    if ($? != 0)
    {
      LogMsg "autoreconf failed\n";
      return undef;
    }
  }

  return $Impacts;
}


#
# Build helpers
#

my $_CPUCount;

sub GetCPUCount()
{
  if (!defined $_CPUCount)
  {
    if (open(my $Fh, "<", "/proc/cpuinfo"))
    {
      # Linux
      map { $_CPUCount++ if (/^processor/); } <$Fh>;
      close($Fh);
    }
    $_CPUCount ||= 1;
  }
  return $_CPUCount;
}

sub BuildNativeTestAgentd()
{
  my $NativeTestAgentd = "$::RootDir/bin/build/testagentd";
  my $Before = GetMTime($NativeTestAgentd);

  InfoMsg "\nBuilding the native testagentd\n";
  my $CPUCount = GetCPUCount();
  system("cd '$::RootDir/src/testagentd' && set -x && ".
         "time make -j$CPUCount build");
  if ($? != 0)
  {
    LogMsg "Build testagentd failed\n";
    return !1;
  }

  if ($Before != GetMTime($NativeTestAgentd))
  {
    LogMsg "Updated testagentd\n";
  }
  return 1;
}

sub BuildWindowsTestAgentd()
{
  my %Files;
  foreach my $Bits ("32", "64")
  {
    $Files{"Path$Bits"} = "$::RootDir/src/testagentd/TestAgent$Bits.exe";
    $Files{"Before$Bits"} = GetMTime($Files{"Path$Bits"});
  }

  InfoMsg "\nRebuilding the Windows TestAgentd\n";
  my $CPUCount = GetCPUCount();
  system("cd '$::RootDir/src/testagentd' && set -x && ".
         "time make -j$CPUCount iso");
  if ($? != 0)
  {
    LogMsg "Build winetestbot.iso failed\n";
    return !1;
  }

  foreach my $Bits ("32", "64")
  {
    if ($Files{"Before$Bits"} != GetMTime($Files{"Path$Bits"}))
    {
      LogMsg "Updated ". basename($Files{"Path$Bits"}) ."\n";
    }
  }
  return 1;
}

sub GetTestLauncher($)
{
  my ($Mission) = @_;

  my $Bits = ($Mission->{Build} =~ /64/) ? "64" : "32";
  return "$::RootDir/src/TestLauncher/TestLauncher$Bits.exe";
}

sub BuildTestLauncher()
{
  my %Files;
  foreach my $Bits ("32", "64")
  {
    $Files{"Path$Bits"} = GetTestLauncher({Build => "win$Bits"});
    $Files{"Before$Bits"} = GetMTime($Files{"Path$Bits"});
  }

  InfoMsg "\nRebuilding TestLauncher\n";
  my $CPUCount = GetCPUCount();
  system("cd '$::RootDir/src/TestLauncher' && set -x && ".
         "time make -j$CPUCount");
  if ($? != 0)
  {
    LogMsg "Build TestLauncher failed\n";
    return !1;
  }

  foreach my $Bits ("32", "64")
  {
    if ($Files{"Before$Bits"} != GetMTime($Files{"Path$Bits"}))
    {
      LogMsg "Updated ". basename($Files{"Path$Bits"}) ."\n";
    }
  }
  return 1;
}

our $FROM_SCRATCH = 1;
our $TRY_WERROR = 2;

sub BuildWine($$$$;$)
{
  my ($TaskMissions, $Flags, $Build, $Configure, $Targets) = @_;
  $Targets ||= [""];

  return 1 if (!$TaskMissions->{Builds}->{$Build});
  # Rebuild from scratch to make sure cruft will not accumulate
  rmtree "$DataDir/wine-$Build" if ($Flags & $FROM_SCRATCH);
  mkdir "$DataDir/wine-$Build" if (!-d "$DataDir/wine-$Build");

  InfoMsg "\nRebuilding the $Build Wine\n";
  my $CPUCount = GetCPUCount();
  my $Rc = 1;
  if (defined $Configure and ($Flags & $TRY_WERROR))
  {
    $Rc = system("cd '$DataDir/wine-$Build' && set -x && ".
                 "time ../wine/configure $Configure --enable-werror && ".
                 join(" && ", map { "time make -j$CPUCount $_" } @$Targets));
    LogMsg "With -Werror the $Build Wine build fails\n" if ($Rc);
  }
  if ($Rc)
  {
    $Rc = system("cd '$DataDir/wine-$Build' && set -x && ".
                 (defined $Configure ? "time ../wine/configure $Configure && " : "").
                 join(" && ", map { "time make -j$CPUCount $_" } @$Targets));
  }
  if ($Rc)
  {
    LogMsg "The $Build Wine build failed\n";
    return !1;
  }

  return 1;
}


#
# Wine addons updates
#

my $ADDONS_URL = "http://dl.winehq.org/wine";

sub _IsAddOnBad($$)
{
  my ($AddOn, $Arch) = @_;

  my $AddOnArch = $AddOn->{$Arch};
  if (!$AddOnArch->{msifile})
  {
    $AddOn->{basedir} = "$DataDir/$AddOn->{name}";
    my $Basename = "wine-$AddOn->{name}-$AddOn->{version}";
    my $ArchSuffix = $Arch eq "" ? "" : "-$Arch";
    $AddOnArch->{msifile} = "$Basename$ArchSuffix.msi";
    $AddOnArch->{shareddir} = $Basename;
    $AddOnArch->{shareddir} .= $ArchSuffix if ($AddOn->{name} ne "mono");
    $AddOnArch->{basetar} = "$Basename$ArchSuffix";
    mkdir "$AddOn->{basedir}";
  }

  return undef if (-d "$AddOn->{basedir}/$AddOnArch->{shareddir}");

  my $Sha256 = Digest::SHA->new(256);
  eval { $Sha256->addfile("$AddOn->{basedir}/$AddOnArch->{msifile}") };
  return "$@" if ($@);

  my $Checksum = $Sha256->hexdigest();
  return undef if ($Checksum eq $AddOnArch->{checksum});
  return "Bad checksum for '$AddOnArch->{msifile}'";
}

sub _UpdateMSIAddOn($$)
{
  my ($AddOn, $Arch) = @_;

  my $AddOnArch = $AddOn->{$Arch};
  InfoMsg "Downloading $AddOnArch->{msifile}\n";
  my $Url="$ADDONS_URL/wine-$AddOn->{name}/$AddOn->{version}/$AddOnArch->{msifile}";
  my $ErrMessage;
  for (1..3)
  {
    system("cd '$AddOn->{basedir}' && set -x && ".
           "wget --no-verbose -O- '$Url' >'$AddOnArch->{msifile}'");
    $ErrMessage = _IsAddOnBad($AddOn, $Arch);
    return 1 if (!defined $ErrMessage);
    unlink "$AddOn->{basedir}/$AddOnArch->{msifile}";
  }
  LogMsg "$ErrMessage\n";
  return 0;
}

sub _UpdateSharedAddOn($$)
{
  my ($AddOn, $Arch) = @_;

  my $AddOnArch = $AddOn->{$Arch};
  InfoMsg "Downloading $AddOnArch->{basetar}\n";
  my $ErrMessage;
  for (1..3)
  {
    foreach my $Suffix (".tar.xz", ".tar.bz2")
    {
      my $Tarfile = "$AddOnArch->{basetar}$Suffix";
      my $Url="$ADDONS_URL/wine-$AddOn->{name}/$AddOn->{version}/$Tarfile";
      system("cd '$AddOn->{basedir}' && set -x && ".
             "wget --no-verbose -O- '$Url' >'$Tarfile'");
      if ($?)
      {
        unlink "$AddOn->{basedir}/$Tarfile";
        $ErrMessage ||= "Could not download the $AddOnArch->{basetar}* archive";
      }
      else
      {
        my $Decompress = $Suffix eq ".tar.xz" ? "xz" : "bzip2";
        system("cd '$AddOn->{basedir}' && set -x && ".
             "$Decompress -c -d '$Tarfile' | tar xf -");
        unlink "$AddOn->{basedir}/$Tarfile";
        if ($?)
        {
          $ErrMessage = "Could not decompress '$Tarfile'";
        }
        elsif (!-d "$AddOn->{basedir}/$AddOnArch->{shareddir}")
        {
          $ErrMessage = "$Tarfile did not contain $AddOnArch->{shareddir}";
        }
        else
        {
          return 1; # Success!
        }
      }
    }
  }

  LogMsg "$ErrMessage\n";
  return 0;
}

sub _UpdateAddOn($$$)
{
  my ($AddOns, $Name, $Arch) = @_;

  my $AddOn = $AddOns->{$Name};
  if (!defined $AddOn)
  {
    LogMsg "Could not get information on the $Name addon\n";
    return 0;
  }
  if (!$AddOn->{version})
  {
    LogMsg "Could not get the $Name version\n";
    return 0;
  }
  if (!$AddOn->{$Arch})
  {
    LogMsg "Could not get the $Name $Arch MSI checksum\n";
    return 0;
  }

  return 1 if (!_IsAddOnBad($AddOn, $Arch));
  return _UpdateSharedAddOn($AddOn, $Arch) || _UpdateMSIAddOn($AddOn, $Arch);
}

sub UpdateAddOns()
{
  my $AddOns;
  my $AddonSrc = "wine/dlls/appwiz.cpl/addons.c";
  if (open(my $fh, "<", "$DataDir/$AddonSrc"))
  {
    my $Arch = "";
    while (my $Line= <$fh>)
    {
      if ($Line =~ /^\s*#\s*define\s+(?:GECKO|MONO)_ARCH\s+"([^"]+)"/)
      {
        $Arch = $1;
      }
      elsif ($Line =~ /^\s*#\s*define\s*(GECKO|MONO)_VERSION\s*"([^"]+)"/)
      {
        my ($AddOn, $Version) = ($1, $2);
        $AddOn =~ tr/A-Z/a-z/;
        $AddOns->{$AddOn}->{name} = $AddOn;
        $AddOns->{$AddOn}->{version} = $Version;
      }
      elsif ($Line =~ /^\s*#\s*define\s*(GECKO|MONO)_SHA\s*"([^"]+)"/)
      {
        my ($AddOn, $Checksum) = ($1, $2);
        $AddOn =~ tr/A-Z/a-z/;
        $AddOns->{$AddOn}->{$Arch}->{checksum} = $Checksum;
        $Arch = "";
      }
    }
    close($fh);
  }
  else
  {
    LogMsg "Could not open '$AddonSrc': $!\n";
    return 0;
  }

  return _UpdateAddOn($AddOns, "gecko", "x86") &&
         _UpdateAddOn($AddOns, "gecko", "x86_64") &&
         _UpdateAddOn($AddOns, "mono",  "x86");
}


#
# Wine helpers
#

sub SetupWineEnvironment($)
{
  my ($Mission) = @_;

  my $BaseName = GetMissionBaseName($Mission);
  $ENV{WINEPREFIX} = "$DataDir/wineprefix-$BaseName";
  $ENV{DISPLAY} ||= ":0.0";

  my $Lang = $Mission->{lang} || "en_US";
  if ($Lang =~ /^[a-zA-Z0-9\@_.-]+$/)
  {
    $Lang =~ s/(@[a-z]*$|$)/.UTF-8$1/ if ($Lang !~ /\./);
    $ENV{LANG} = $Lang;
  }

  return $BaseName;
}

sub GetWineDir($)
{
  my ($Mission) = @_;
  return "$DataDir/wine-$Mission->{Build}";
}

sub GetTestDir($)
{
  my ($Mission) = @_;

  my $DevDir = "$ENV{WINEPREFIX}/dosdevices";
  return "." if (!-d $DevDir);

  my $Dir = $Mission->{dir} || "";
  # We cannot put colons in missions so restore the drive letter
  $Dir = "$DevDir/c:/$Dir" if ($Dir !~ s~^([a-z])/~$DevDir/$1:/~);
  return -d $Dir ? $Dir : "$DevDir/c:";
}

sub RunWine($$$)
{
  my ($Mission, $Cmd, $CmdArgs) = @_;

  my $TestDir = GetTestDir($Mission);

  my $WineDir = GetWineDir($Mission);
  $Cmd = "$WineDir/$Cmd" if ($Cmd =~ /\.exe\.so$/);
  my $Magic = `file '$Cmd'`;
  my $Wine = ($Magic =~ /ELF 64/ ? "$WineDir/wine64" : "$WineDir/wine");

  return system("set -x && cd '$TestDir' && ".
                "time '$Wine' '$Cmd' $CmdArgs");
}


#
# WinePrefix helpers
#

sub CreateWinePrefix($;$)
{
  my ($Mission, $Wait) = @_;

  return "\$WINEPREFIX is not set!" if (!$ENV{WINEPREFIX});
  rmtree($ENV{WINEPREFIX});

  # Crash dialogs cause delays so disable them
  if (RunWine($Mission, "reg.exe", "ADD HKCU\\\\Software\\\\Wine\\\\WineDbg /v ShowCrashDialog /t REG_DWORD /d 0"))
  {
    return "Failed to disable the crash dialogs: $!";
  }

  if ($Wait)
  {
    # Ensure the WinePrefix has been fully created and the registry files
    # saved before returning.
    my $WineDir = "$DataDir/wine-$Mission->{Build}";
    system("'$WineDir/server/wineserver' -w");
  }

  return undef;
}

1;
